### Notes

Installed node, gulp and bower.

To install the Ionic framework, at the prompt type:
```
npm install cordova ionic -g
```

#### Creating an Ionic Project

To scaffold out a new Ionic project, type:
```
ionic start con-fusion-v3 sidemenu
```
Move to the con-fusion-v3 folder and type:
```
ionic serve --lab
```

#### Installing json-server

Install json-server globally by typing:
```
npm install json-server -g
```

Create a new folder named json-server, and move to this folder. After creating the db.json file, type:
```
json-server --watch db.json
```