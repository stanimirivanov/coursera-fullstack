### Notes

#### Installing and using the lessc Compiler

Install the node module to support the compilation of the Less file:

```
npm install -g less
```

Go to the CSS folder and type the following to compile the Less file into a CSS file:

```
lessc mystyles.less > mystyles.css
```

#### Installing Bower

Install Bower as a global node module:

```
npm install -g bower
```

#### Creating bower.son File

Create and initialize a _bower.json_ file:

```
bower init
```

Run 
```
bower install
```

to let Bower fetch Bootstrap, JQuery, Font Awesome and Angular files and put them into the bower_components folder.