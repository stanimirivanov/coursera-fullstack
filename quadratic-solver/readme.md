##### Using the "prompt" Node Module

To check for the availability of the node module named "prompt", type the following at the command prompt:
      
```
npm search prompt
```

This will list a number of NPM modules with the prompt in the name or description.

Next we create the _package.json_ file using NPM. To do this, type the following at the prompt:

```
npm init
```

Answer the questions that NPM asks to fill in some information to the _package.json_ file.

To install the "prompt" module, type the following at the command prompt:

```
npm install prompt -S
```

This will install the prompt module and also save this dependency in the _package.json_ file.