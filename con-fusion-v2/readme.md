### Notes

#### Install the lessc Compiler

Run 
```
bower install
```

to let Bower fetch Bootstrap, JQuery, Font Awesome and Angular files and put them into the bower_components folder.

#### Install Grunt

This will install the Grunt CLI globally
```
npm install -g grunt-cli
```

Install Grunt  to use within this project
```
npm install grunt --save-dev
```

#### Install the JSHint Task

Install the following modules
```
npm install grunt-contrib-jshint --save-dev
npm install jshint-stylish --save-dev
npm install time-grunt --save-dev
npm install jit-grunt --save-dev
```
The first one installs the Grunt module for JSHint, 
and the second one adds further to print out the messages from JSHint in a better format. 
The time-grunt module generates time statistics about how much time each task consumes, 
and jit-grunt includes the necessary downloaded Grunt modules when needed for the tasks.

#### Install Grunt tasks for Copying the Files and Cleaning Up the Dist Folder

```
npm install grunt-contrib-copy --save-dev
npm install grunt-contrib-clean --save-dev
```

#### Install Grunt tasks for Preparing the Distribution Folder and Files

```
npm install grunt-contrib-concat --save-dev
npm install grunt-contrib-cssmin --save-dev
npm install grunt-contrib-uglify --save-dev
npm install grunt-filerev --save-dev
npm install grunt-usemin --save-dev
```

#### Install Grunt tasks for Watch, Connect and Serve Tasks

```
npm install grunt-contrib-watch --save-dev
npm install grunt-contrib-connect --save-dev
```

#### Run Grunt Task

Building
```
grunt
```

Building and watching
```
grunt serve
```

#### Install Gulp
```
npm install -g gulp
npm install gulp --save-dev
```

#### Install Gulp Plugins

```
npm install jshint
npm install gulp-jshint
npm install jshint-stylish
npm install gulp-imagemin
npm install gulp-concat
npm install gulp-uglify
npm install gulp-minify-css
npm install gulp-usemin
npm install gulp-cache
npm install gulp-changed
npm install gulp-rev
npm install gulp-rename
npm install gulp-notify
npm install browser-sync
npm install del --save-dev
npm install gulp-ng-annotate --save-dev
npm install gulp-foreact --save-dev
```

#### Run Gulp Task

Building
```
gulp
```

Building and watching
```
gulp watch
```

#### Install and run json-server

```
npm install json-server -g
```
```
json-server --watch db.json
```

#### Unit Testing

```
npm install jasmine-core --save-dev
npm install karma-cli -g
npm install karma-jasmine --save-dev
npm install phantomjs --save-dev
npm install karma-phantomjs-launcher --save-dev
npm install karma-chrome-launcher --save-dev
npm install protractor -g
webdriver-manager update
```

```
karma start karma.conf.js
```

#### E2E Testing

```
npm install protractor -g
webdriver-manager update
```

Make sure you have started the json-server
```
json-server --watch db.json
```
Serve the application
```
gulp watch
```
Run e2e tests
```
protractor protractor.conf.js
```

