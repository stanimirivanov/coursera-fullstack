### Useful Links

#### Full Stack Web Development

- [What is a Full Stack developer?](http://www.laurencegellert.com/2012/08/what-is-a-full-stack-developer/)
- [Wait, Wait… What is a Full-stack Web Developer After All?](http://edward-designer.com/web/full-stack-web-developer/)
- [The Myth of the Full-stack Developer](http://andyshora.com/full-stack-developers.html)
- [What is the 3-Tier Architecture?](http://www.tonymarston.net/php-mysql/3-tier-architecture.html)

#### Bootstrap

- [Bootstrap Home Page](http://getbootstrap.com/)
- [Bootstrap typography](http://getbootstrap.com/css/#type)
- [The 5 Most Popular Frontend Frameworks of 2014 Compared](http://www.sitepoint.com/5-most-popular-frontend-frameworks-compared/)
- [Front-end Frameworks v2.5](http://usablica.github.io/front-end-frameworks/) and [comparison](http://usablica.github.io/front-end-frameworks/compare.html)

#### Responsive Design and Bootstrap Grid System

- [Bootstrap Grid System
- [The Subtle Magic Behind Why the Bootstrap 3 Grid Works](http://www.helloerik.com/the-subtle-magic-behind-why-the-bootstrap-3-grid-works)
- [What The Heck Is Responsive Web Design?](http://johnpolacek.github.io/scrolldeck.js/decks/responsive/)
- [Beginner’s Guide to Responsive Web Design](http://blog.teamtreehouse.com/beginners-guide-to-responsive-web-design)
- [The 2014 Guide to Responsive Web Design](http://blog.teamtreehouse.com/modern-field-guide-responsive-web-design)


#### Navigation and Navigation Bar

- [Navbar](http://getbootstrap.com/components/#navbar)
- [Breadcrumbs](http://getbootstrap.com/components/#breadcrumbs)

- [Accessible Rich Internet Applications (ARIA)](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA)

#### Information Architecture

- [Information Architecture 101: Techniques and Best Practices](http://sixrevisions.com/usabilityaccessibility/information-architecture-101-techniques-and-best-practices/)
- [Web Site Information Architecture models](http://webdesignfromscratch.com/website-architecture/ia-models/)
- [What is information architecture?](http://www.steptwo.com.au/papers/kmc_whatisinfoarch/)
- [Information Architecture Tutorial](http://www.webmonkey.com/2010/02/Information_Architecture_Tutorial/)

#### Navigation Bar Design

- [Designing A Winning Navigation Menu: Ideas and Inspirations](http://www.hongkiat.com/blog/navigation-design-ideas-inspiration/)
- [Are You Making These Common Website Navigation Mistakes?](https://blog.kissmetrics.com/common-website-navigation-mistakes/)
- [3 Reasons We Should Stop Using Navigation Bars](http://www.webdesignerdepot.com/2014/01/3-reasons-we-should-stop-using-navigation-bars/)

#### Breadcrumbs

- [Breadcrumb Navigation Examined: Best Practices & Examples](http://www.hongkiat.com/blog/breadcrumb-navigation-examined-best-practices-examples/)
- [Breadcrumb Navigation: A Guide On Types, Benefits And Best Practices](http://blog.woorank.com/2014/11/breadcrumb-navigation-guide/)

#### Icon Fonts

- [Why And How To Use Icon Fonts](http://vanseodesign.com/web-design/icon-fonts/)
- [Icon Fonts are Awesome](https://css-tricks.com/examples/IconFont/)
- [FontAwesome](https://fortawesome.github.io/Font-Awesome/)
- [Get started with FontAwesome](https://fortawesome.github.io/Font-Awesome/get-started/)
- [Bootstrap-Social](http://lipis.github.io/bootstrap-social/)
- [The Final Nail in the Icon Fonts Coffin?](http://www.sitepoint.com/final-nail-icon-fonts-coffin/)
- [Using SVGs](http://gomakethings.com/using-svgs/)

#### User Input

- [Bootstrap Buttons](http://getbootstrap.com/css/#buttons)
- [Bootstrap Button Groups](http://getbootstrap.com/components/#btn-groups)
- [Bootstrap Forms](http://getbootstrap.com/css/#forms)
- [The Difference Between Anchors, Inputs and Buttons](https://davidwalsh.name/html5-buttons)
- [When To Use The Button Element](https://css-tricks.com/use-button-element/)

#### Displaying Content

- [Bootstrap Tables](http://getbootstrap.com/css/#tables)
- [Bootstrap Panels](http://getbootstrap.com/components/#panels)
- [Bootstrap Wells](http://getbootstrap.com/components/#wells)
- [Bootstrap Blockquote](http://getbootstrap.com/css/#type-blockquotes)

#### Images and Media

- [Bootstrap Image Classes](http://getbootstrap.com/css/#images)
- [Bootstrap Thumbnail Classes](http://getbootstrap.com/components/#thumbnails)
- [Bootstrap Media Object Classes](http://getbootstrap.com/components/#media)
- [Bootstrap Responsive Embed Classes](http://getbootstrap.com/components/#responsive-embed)

#### Alerting Users

- [Bootstrap Labels](http://getbootstrap.com/components/#labels)
- [Bootstrap Badges](http://getbootstrap.com/components/#badges)
- [Bootstrap Alerts](http://getbootstrap.com/components/#alerts)
- [Bootstrap Progress Bars](http://getbootstrap.com/components/#progress)

#### Bootstrap JavaScript Components

- [Bootstrap and JavaScript](http://getbootstrap.com/javascript/)
- [Bootstrap JS Data Attributes](http://getbootstrap.com/javascript/#js-data-attrs)

#### Tabs and Tabbed Navigation

- [Bootstrap Navs](http://getbootstrap.com/components/#nav)
- [Bootstrap Tabs](http://getbootstrap.com/components/#nav-tabs)
- [Bootstrap Pills](http://getbootstrap.com/components/#nav-pills)
- [Bootstrap Togglable Tabs Javascript plugin](http://getbootstrap.com/javascript/#tabs)

#### Hide and Seek

- [Bootstrap Collapse Plugin](http://getbootstrap.com/javascript/#collapse)
- [Bootstrap Accordion](http://getbootstrap.com/javascript/#collapse-example-accordion)
- [Bootstrap Scrollspy](http://getbootstrap.com/javascript/#scrollspy)
- [Bootstrap Affix](http://getbootstrap.com/javascript/#affix)

#### Revealing Content

- [Bootstrap Tooltips](http://getbootstrap.com/javascript/#tooltips)
- [Bootstrap Popovers](http://getbootstrap.com/javascript/#popovers)
- [Bootstrap Modals](http://getbootstrap.com/javascript/#modals)

#### Carousel

- [Bootstrap Carousel](http://getbootstrap.com/javascript/#carousel)

#### Bootstrap and JQuery

- [Bootstrap Carousel Methods](http://getbootstrap.com/javascript/#carousel-methods)
- [JQuery](http://jquery.com/)
- [W3Schools JQuery](http://www.w3schools.com/jquery/)

#### Node.js and NPM

- [Nodejs.org](https://nodejs.org/)
- [Npmjs.com](https://www.npmjs.com/)
- [Node API Documentation](https://nodejs.org/api/)
- [NPM Documentation](https://docs.npmjs.com/)

#### CSS Preprocessors: Additional Resources

- [Less Getting Started](http://lesscss.org/)
- [Sass Basics](http://sass-lang.com/guide)
- [Getting Started with Less Tutorial](https://scotch.io/tutorials/getting-started-with-less)
- [Getting Started with Sass Tutorial](https://scotch.io/tutorials/getting-started-with-sass)

#### Web Tools: Bower

- [Bower Website](http://bower.io/)
- [Bower 101](https://medium.com/@ZaidHanania/bower-101-c0b57322df8)
- [Node, Grunt, Bower and Yeoman - A Modern web dev's Toolkit](http://juristr.com/blog/2014/08/node-grunt-yeoman-bower/)

#### Introduction to AngularJS

- [Angular Official Website](https://angularjs.org/)
- [Angular Documentation](https://docs.angularjs.org/guide)
- [What is AngularJS?](https://docs.angularjs.org/guide/introduction)
- [Angular Data Binding](https://docs.angularjs.org/guide/databinding)
- Angular Directives: [ng-app](https://docs.angularjs.org/api/ng/directive/ngApp), [ng-init](https://docs.angularjs.org/api/ng/directive/ngInit), [ng-model](https://docs.angularjs.org/api/ng/directive/ngModel), [ng-repeat](https://docs.angularjs.org/api/ng/directive/ngRepeat)
- [Choosing the right JavaScript framework for the job](https://www.lullabot.com/articles/choosing-the-right-javascript-framework-for-the-job)
- [Should You Use a JavaScript MVC Framework to Build Your Web Application?](https://www.codementor.io/javascript/tutorial/should-you-build-your-web-application-with-javascript-mvc-frameworks)
- [AngularJS Critique](http://tutorials.jenkov.com/angularjs/critique.html)
- [Declarative vs. Imperative Programming for the Web](http://codenugget.co/2015/03/05/declarative-vs-imperative-programming-web.html)

#### The Model View Controller Framework

- [Angular Concepts](https://docs.angularjs.org/guide/concepts)
- [Angular Module](https://docs.angularjs.org/guide/module)
- [Angular Controllers](https://docs.angularjs.org/guide/controller)
- [Model-View-Whatever](http://www.beyondjava.net/blog/model-view-whatever/)

#### Angular Filters

- [List of Angular Built-in Filters](https://docs.angularjs.org/api/ng/filter)
- [Currency Filter](https://docs.angularjs.org/api/ng/filter/currency)
- [Date Filter](https://docs.angularjs.org/api/ng/filter/date)
- [Filter](https://docs.angularjs.org/api/ng/filter/filter)
- [OrderBy Filter](https://docs.angularjs.org/api/ng/filter/orderBy)